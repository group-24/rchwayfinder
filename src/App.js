import React, { Component } from 'react';
import Graph from'./components/graph';
import Menu from './components/menu';

let styles = {
  app : {

  },

  graph: {
    height: "100%",
    width: "79%",
    display:"inline-block",
    float: "right",

  },

  menu: {
    height: "100%",
    width: "20%",
    display:"inline-block",
    float: "left",
    marginRight: "0.01%",


  }


}

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      display: "inline-block",
      width: 0,
      height: 0,
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    return (
      <div className="App" style={this.state}>
        <div id="menu" style={styles.menu}>
          <Menu/>
        </div>
        <div id="graph" style={styles.graph}>
          <Graph/>
        </div>

      </div>
    );
  }
}

export default App;
