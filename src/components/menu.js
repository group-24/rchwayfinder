import React, {
  Component
} from 'react';
import Radium from 'radium';
import PropTypes from 'prop-types';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';


const styles = ({
  menuItem: {
    '&:focus': {
      backgroundColor: "white",
      '& $primary, & $icon': {
        color: "grey",
      },
    },
  },
  primary: {},
  icon: {},
  button: {
    margin : "10px",
  }
});

class Menu extends Component {

  constructor(props){
    super(props);
    this.state = {
        menuItem: {
          '&:focus': {
              backgroundColor: "white",
              color: "blue"
            }
        },
        icon: "",
        primary: "primary",
        // menuComponent: building,
    };

  }


  // setMenu = (curMenu) => {
  //   switch(curMenu){
  //     case building:
  //
  //     case default:
  //       console.err("reached default in setMenu");
  //   }
  // }



  render(){

    const{classes} = styles;

    return(
      <div>
        <Paper>
          <MenuList>
            <MenuItem >
            <ListItemIcon >
              <SendIcon />
            </ListItemIcon>
            <ListItemText  inset primary="Sent mail" />
            </MenuItem>
            <MenuItem >
                      <ListItemIcon >
              <DraftsIcon />
            </ListItemIcon>
            <ListItemText  inset primary="Drafts" />
          </MenuItem>
          <MenuItem>
            <ListItemIcon >
              <InboxIcon />
            </ListItemIcon>
            <ListItemText  inset primary="Inbox" />
          </MenuItem>
          </MenuList>
        </Paper>


      <Button variant="outlined" color="primary" style={styles.button}>
        Building Menu </Button>
      <Button variant="outlined" color="primary" style={styles.button}
              // onClick={setMenu(floor)}
              >
        Floor Menu </Button>
    </div>
    );
  }
}



export default Radium(Menu)
